
int matrix3x3_mul(float *matrix1, float *matrix2, float *matrix3)
{
  for(int i=0; i<3;i++)
      for(int j=0; j<3;j++)
	{
	  matrix3[i+j*3] = matrix1[0+3*j]*matrix2[i+0] + matrix1[1+3*j]*matrix2[i+3] + matrix1[2+3*j]*matrix2[i+6];
	}
  return 0;
}
