CXX=$(ARMFLAGS)

all: riscv

riscv: matrix_riscv double_pendulum

matrix_riscv: main.o matrix_mul.o print.o
	$(CC) $(CXX) $(OBJ)/main.o $(OBJ)/matrix_mul.o $(OBJ)/print.o

main.o:
	$(CC) $(CXX) -c -o $(OBJ)/main.o main.c

matrix_mul.o:
	$(CC) $(CXX) -c -o $(OBJ)/matrix_mul.o matrix_mul.c

double_pendulum:
	$(CC) $(CXX) -c -o $(OBJ)/double_pendulum.o double_pendulum.c

print.o:
	$(CC) $(CXX) -c -o $(OBJ)/print.o print.c

clean:
	rm $(OBJ)/*
