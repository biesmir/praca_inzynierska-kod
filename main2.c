/* #include "matrix.h" */
#include <stdio.h>
int matrix3x3_mul(float *matrix1, float *matrix2, float *matrix3)
{
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++){
      matrix3[i+j*3] = 0;
      for(int k=0; k<3; k++)
	  matrix3[i+j*3] += matrix1[k+3*j]*matrix2[i+k*3];
	}
  return 0;
}

int main(){
  float matrix1[9] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
  float matrix2[9] = {1,2,3,1,2,3,1,2,3};
  float matrix3[9];
  matrix3x3_mul(matrix1, matrix2, matrix3);
  for(int i; i<9;i++)
    printf("%f\n", matrix3[i]);
  return 0;

}
