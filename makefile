RVCC=/usr/bin/riscv64-linux-gnu-gcc
ARMCC=~/toolchain/gcc-arm-none-eabi-8-2019-q3-update/bin/arm-none-eabi-gcc
RVOBJ=./obj/riscv
ARMOBJ=./obj/arm
all:
	make CC=$(RVCC) OBJ=$(RVOBJ) -f Makefile -j8
	make CC=$(ARMCC) OBJ=$(ARMOBJ) ARMFLAGS=--specs=nosys.specs -f Makefile -j8 
clean:
	rm $(RVOBJ)/*
	rm $(ARMOBJ)/*
